# An OpenFOAM utility to write the position of each mesh cell center for a given timestep. 

This tool is similar to the writeCellCentres utility (which outputs three files, one with the x, y, and z components, respectively), except that it outputs a single file with the vector coordinates of the cell center positions.

## Installation

To install the utility: 

~~~bash
$ mkdir -p $WM_PROJECT_USER_DIR/applications/utilities/postProcessing/miscellaneous
$ cd $WM_PROJECT_USER_DIR/applications/utilities/postProcessing/miscellaneous
$ git clone https://peterjvonk@bitbucket.org/peterjvonk/cellcenters.git cellCenters
$ cd cellCenters
$ wmake
~~~

The utility will be compiled and located in $FOAM_USER_APPBIN

## Usage

~~~bash
$ cellCenters -help
~~~

To extract cell centers for the latest time only use:

~~~bash
$ cellCenters -latestTime
~~~

To extract cell centers for each time step in parallel use:

~~~bash
$ mpirun -np <number-of-processors> cellCenters -parallel 
~~~

## Sample output

You'll get an output consisting of the 3D coordinates of each cell center, similar to this: 

~~~C++
/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.1                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    location    "0.007";
    object      cellCenters;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [0 1 0 0 0 0 0];

internalField   nonuniform List<vector> 
100
(
(-4.95 0 0)
(-4.85 0 0)
    ...
(4.95 0 0)
)
;

boundaryField
{
    sides
    {
        type            sliced;
        value           nonuniform List<vector> 2((5 0 0) (-5 0 0));
    }
    empty
    {
        type            sliced;
        value           nonuniform 0();
    }
}


// ************************************************************************* //
~~~